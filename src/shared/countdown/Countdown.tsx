import { component$ } from '@builder.io/qwik';

export const Countdown = component$(() => {
  const timerClass: string =
    'bg-gray-blue text-white text-sm px-1.5 rounded-md inline-block';

  return (
    <p class="flex gap-1">
      <span class={timerClass}>02</span>
      <span class="text-gray-blue">:</span>
      <span class={timerClass}>03</span>
      <span class="text-gray-blue">:</span>
      <span class={timerClass}>47</span>
    </p>
  );
});
