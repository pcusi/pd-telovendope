import type { ProductResponse } from '@/components/index/hot-sale/HotSale';
import { CardType } from '@/infrastructure/enum/CardType';
import { get } from 'lodash';
import { JSX } from '@builder.io/qwik/jsx-runtime';

export interface ProductCardProps {
  product?: ProductResponse;
  type: string;
}

export const renderProductCardByType = (
  type: string,
  product?: ProductResponse,
): JSX.Element => {
  switch (type) {
    case CardType.TOP_CARD:
      return (
        <div class="border border-gray-200 rounded-xl flex flex-col gap-4 p-4 items-center lg:items-start">
          <div class="lg:max-h-[238px]">
            <img
              src={get(product, 'img')}
              alt="data-test"
              class="object-fit w-full h-full"
            />
          </div>
          <div class="flex flex-col items-center lg:items-start">
            <h2 class="text-gray font-thin text-sm">{get(product, 'title')}</h2>
            <p class="text-gray-400 text-sm">
              ${get(product, 'price', 0).toString()}
            </p>
          </div>
        </div>
      );
    case CardType.TOP_CARD_HORIZONTAL:
      return (
        <div class="bg-gray-200/[.2] p-10 rounded-xl flex lg:flex-row flex-col-reverse justify-between">
          <div class="flex flex-col lg:gap-0 gap-10 lg:items-center items-start justify-between">
            <div class="flex flex-col gap-2">
              <h1 class="text-black font-semibold text-xl">iPad M1</h1>
              <p class="text-gray-400 font-thin">$27.42</p>
            </div>
            <div class="lg:mt-auto ml-auto">
              <div class="flex lg:flex-row items-center">
                <p class="text-black text-sm font-semibold">More Details</p>
              </div>
            </div>
          </div>
          <div class="lg:max-h-[240px] lg:max-w-[240px]">
            <img
              src="/assets/img/telovendope_imac_m2.webp"
              class="w-full h-full object-contain"
              alt="Telovendope iMac M2"
            />
          </div>
        </div>
      );
    default:
      return (
        <div class="border border-gray-200 rounded-xl flex flex-col gap-4 p-4 items-center lg:items-start">
          <div class="lg:max-h-[140px] bg-gray-200/[.2] rounded-lg">
            <img
              src={get(product, 'img')}
              alt="data-test"
              class="object-fit w-full h-full"
            />
          </div>
          <div class="flex flex-col items-center lg:items-start">
            <h2 class="text-black font-thin text-sm">
              {get(product, 'title')}
            </h2>
            <p class="text-orange-400 text-sm">
              ${get(product, 'price', 0).toString()}
            </p>
          </div>
        </div>
      );
  }
};

export const ProductCard = ({ product, type }: ProductCardProps) => {
  return renderProductCardByType(type, product);
};
