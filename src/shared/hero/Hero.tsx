import { component$ } from '@builder.io/qwik';
import { HiChevronRightOutline } from '@qwikest/icons/heroicons';

export const Hero = component$(() => {
  return (
    <div class="w-full rounded-lg bg-gradient-to-r from-white via-[#E4E8EF] via-21% via-[#FCF6F3] via-31% via-[#FCFAF5] via-51% to-white my-10 p-20">
      <div class="grid xl:grid-cols-2 grid-cols-1 gap-8">
        <div class="inline-flex flex-col xl:items-start items-center gap-4">
          <span class="bg-amber-100 text-amber-600 px-2 py-0.5 rounded-lg text-xs font-semibold">
            Opening sale discount 50%
          </span>
          <h1 class="text-black xl:text-4xl xl:text-left text-center font-bold">
            Life Advice Looking Through A Window
          </h1>
          <p class="text-black text-sm xl:text-left text-center font-regular mb-6">
            It won’t be a bigger problem to find one video game lover in your
            neighbor
          </p>
          <button class="bg-black rounded-lg text-white text-sm px-5 py-3 flex flex-row gap-4 items-center">
            Shop Now
            <HiChevronRightOutline class="text-white text-xl" />
          </button>
        </div>
        <div class="flex flex-col items-center">
          <div class="max-w-[400px] max-h-[400px]">
            <img
              src="/assets/img/telovendope_apple_watch_ultra.webp"
              alt="rstore"
              class="object-fit w-full h-full"
            />
          </div>
        </div>
      </div>
    </div>
  );
});
