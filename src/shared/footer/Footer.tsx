import { component$ } from '@builder.io/qwik';

export const Footer = component$(() => {
  return (
    <div class="flex gap-10 bg-gray-200/[.8] p-16">
      <div class="grid lg:grid-cols-2 grid-cols-1 gap-4">
        <div class="flex flex-col items-start">
          <div class="max-h-[40px]">
            <img
              src="/assets/img/telovendope_logotype.svg"
              class="object-contain w-full h-full"
              alt="telovendo.pe logotype"
            />
          </div>
        </div>
        <div class="grid lg-grid-cols-3">
          <ul class="list-none list-inside">
            <li>Terms & conditions</li>
            <li>Terms & conditions</li>
          </ul>
        </div>
      </div>
    </div>
  );
});
