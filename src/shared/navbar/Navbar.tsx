import { component$ } from '@builder.io/qwik';
import {
  HiHeartOutline,
  HiShoppingCartOutline,
  HiUserOutline,
  HiBars4Outline,
  HiMagnifyingGlassOutline,
} from '@qwikest/icons/heroicons';
import { twMerge } from 'tailwind-merge';

export const Navbar = component$(() => {
  const iconColor: string = 'text-[#212B36] xl:text-xl text-base cursor-pointer';

  return (
    <header class="bg-gradient-to-r from-white via-[#E4E8EF] via-21% via-[#FCF6F3] via-31% via-[#FCFAF5] via-51% to-white w-full">
      <div class="container flex lg:flex-row lg:justify-between lg:py-9">
        <div class="flex lg:flex-row gap-10 items-center">
          <div class="flex lg:flex-row gap-2 items-center">
            <HiBars4Outline class={iconColor} />
            <p class="text-[#212B36] text-base">Categories</p>
          </div>
          <div class="flex flex-row gap-2 py-3 px-4 items-center bg-white xl:max-w-[320px] xl:w-[320px] rounded-lg">
            <HiMagnifyingGlassOutline class={twMerge(iconColor, 'text-[#919EAB]')} />
            <input
              type="text"
              placeholder="Search..."
              class="block w-full
              focus:border-transparent focus:ring-0 border-0 text-[#919EAB]
              xl:text-base text-sm placeholder-[#919EAB]"
            />
          </div>
        </div>
        <div class="flex lg:flex-row gap-6 items-center">
          <HiHeartOutline class={iconColor} />
          <HiShoppingCartOutline class={iconColor} />
          <HiUserOutline class={iconColor} />
        </div>
      </div>
    </header>
  );
});
