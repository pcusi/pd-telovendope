import { ProductResponse } from '@/components/index/hot-sale/HotSale';
import { useStore } from '@builder.io/qwik';
import { ReviewResponse } from '@/components/index/reviews/Reviews';
import { v4 } from 'uuid';
import { format } from 'date-fns';
import { DateFormat } from '@/infrastructure/enum/DateFormat';

export interface UseMainStateProps {
  products: ProductResponse[];
  reviews: ReviewResponse[];
}

export const useMainState = (): UseMainStateProps => {
  const store: UseMainStateProps = useStore<UseMainStateProps>({
    products: [
      {
        img: '/assets/img/telovendope_ps5_controller.webp',
        price: 27.42,
        title: 'PS5 Controller',
      },
      {
        img: '/assets/img/telovendope_ps5.png',
        price: 945,
        title: 'PS5 - Combo',
      },
      {
        img: '/assets/img/telovendope_ipad.webp',
        price: 1149.9,
        title: 'Apple Ipad M2',
      },
      {
        img: '/assets/img/telovendope_mac_mini_m2.webp',
        price: 899.99,
        title: 'Mac Mini M2',
      },
      {
        img: '/assets/img/telovendope_apple_magic_kyb.png',
        price: 200,
        title: 'Magic Keyboard',
      },
      {
        img: '/assets/img/telovendope_imac_m2.webp',
        price: 1499.99,
        title: 'iMac M2',
      },
    ],
    reviews: [
      {
        id: v4(),
        title: format(new Date(), DateFormat.DD_LL_YYYY),
        comment:
          'Now, if you are interested in being the best player, getting really good money',
        user: 'Daniela Prudencio',
        rating: 5,
      },
      {
        id: v4(),
        title: format(new Date(), DateFormat.DD_LL_YYYY),
        comment:
          'Now, if you are interested in being the best player, getting really good money',
        user: 'Piero Cusi',
        rating: 2,
      },
      {
        id: v4(),
        title: format(new Date(), DateFormat.DD_LL_YYYY),
        comment:
          'Now, if you are interested in being the best player, getting really good money',
        user: 'Fabricio Ramirez',
        rating: 0,
      },
      {
        id: v4(),
        title: format(new Date(), DateFormat.DD_LL_YYYY),
        comment:
          'Now, if you are interested in being the best player, getting really good money',
        user: 'Paloma Ramirez',
        rating: 1,
      },
    ],
  });

  return {
    products: store.products,
    reviews: store.reviews,
  };
};
