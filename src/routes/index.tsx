import { component$ } from '@builder.io/qwik';
import type { DocumentHead } from '@builder.io/qwik-city';
import { DocumentHeadTitle } from '@/infrastructure/enum/DocumentHeadTitle';
import { Navbar } from '@/shared/navbar/Navbar';
import { Hero } from '@/shared/hero/Hero';
import { HotSale } from '@/components/index/hot-sale/HotSale';
import { useMainState, UseMainStateProps } from '@/state/index/useMainState';
import { Featured } from '@/components/index/featured/Featured';
import { SpecialOffer } from '@/components/index/special-offer/SpecialOffer';
import { Footer } from '@/shared/footer/Footer';
import { Reviews } from '@/components/index/reviews/Reviews';
import { TopSale } from '@/components/index/top-sale/TopSale';

export default component$(() => {
  const { products, reviews }: UseMainStateProps = useMainState();

  return (
    <>
      <Navbar />
      <div class="lg:container lg:p-4">
        <Hero />
        <HotSale products={products} />
        <Featured products={products} />
        <SpecialOffer />
        <TopSale products={products} />
        <Reviews reviews={reviews} />
      </div>
      <Footer />
    </>
  );
});

export const head: DocumentHead = {
  title: DocumentHeadTitle.INDEX,
  meta: [
    {
      name: 'description',
      content: 'Qwik site description',
    },
  ],
};
