import { Countdown } from '@/shared/countdown/Countdown';
import { ProductResponse } from '@/components/index/hot-sale/HotSale';
import { ProductCard } from '@/shared/product/ProductCard';
import { CardType } from '@/infrastructure/enum/CardType';

export interface FeaturedProps {
  products: ProductResponse[];
}

export const Featured = ({ products }: FeaturedProps) => {
  return (
    <>
      <div class="flex flex-col gap-10 lg:my-[120px] lg:p-0 p-4">
        <h1 class="text-black text-2xl lg:text-left text-center font-semibold">
          Featured Products
        </h1>
        <section class="grid lg:grid-cols-3 gap-4">
          <div class="bg-amber-100 p-6 flex flex-col items-center gap-4 rounded-xl">
            <div class="max-w-[320px] max-h-[320px]">
              <img
                src="/assets/img/telovendope_apple_watch_ultra.webp"
                alt=""
                class="object-fit h-full w-full"
              />
            </div>
            <h1 class="text-amber-600 text-sm">
              Sony PlayStation® DualSense™ Wireless Controller
            </h1>
            <h2 class="text-amber-600 font-bold text-base">From $699</h2>
            <Countdown />
          </div>
          <div class="bg-violet-100 p-6 flex flex-col items-center gap-4 rounded-xl">
            <div class="max-w-[320px] max-h-[320px]">
              <img
                src="/assets/img/telovendope_imac_m2.webp"
                alt=""
                class="object-fit h-full w-full"
              />
            </div>
            <h1 class="text-violet-600 text-sm">
              Sony PlayStation® DualSense™ Wireless Controller
            </h1>
            <h2 class="text-violet-600 font-bold text-base">From $1499</h2>
            <Countdown />
          </div>
          <div class="grid grid-cols-2 gap-4">
            {products.slice(0, 4).map((product: ProductResponse) => (
              <ProductCard
                product={product}
                type={CardType.NORMAL_CARD}
                key={Math.random()}
              />
            ))}
          </div>
        </section>
      </div>
    </>
  );
};
