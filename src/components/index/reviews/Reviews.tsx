import { RatingStar } from '@/shared/rating-star/RatingStar';

export type ReviewResponse = {
  id: string;
  comment: string;
  rating: number;
  title: string;
  user: string;
};

export interface ReviewsProps {
  reviews: ReviewResponse[];
}

export const Reviews = ({ reviews }: ReviewsProps) => {
  return (
    <div class="flex flex-col gap-10 lg:my-[120px] lg:p-0 p-4">
      <h1 class="text-black font-bold text-2xl">Popular Reviews</h1>
      <div class="grid lg:grid-cols-4 lg:gap-4 grid-cols-1 gap-2">
        {reviews.map((review: ReviewResponse) => (
          <div
            class="flex flex-col bg-gray-200/[.2] p-4 gap-2 rounded-xl"
            key={review.id}
          >
            <p class="text-gray-400 text-xs font-thin">{review.title}</p>
            <p class="text-black text-sm font-bold">{review.user}</p>
            <div class="flex items-center flex-row gap-0.5">
              {Array(review.rating)
                .fill(review.rating)
                .map(() => (
                  <RatingStar rated={true} key={Math.random()} />
                ))}
              {review.rating !== 5 &&
                Array(5 - review.rating)
                  .fill(5 - review.rating)
                  .map(() => <RatingStar rated={false} key={Math.random()} />)}
            </div>
            <p class="text-gray-400 text-sm font-thin">{review.comment}</p>
          </div>
        ))}
      </div>
    </div>
  );
};
