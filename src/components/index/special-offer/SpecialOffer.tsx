export const SpecialOffer = () => {
  return (
    <div class="flex flex-col gap-10 lg:my-[120px] lg:p-0 p-4">
      <h1 class="text-black text-2xl lg:text-left text-center font-semibold">
        Special Offer
      </h1>
      <section class="grid lg:grid-cols-3 grid-cols-1 gap-16">
        <div class="bg-white rounded-lg shadow-xl flex flex-col justify-center p-10 items-center gap-6">
          <h1 class="text-amber-500 font-bold">New 2023</h1>
          <p class="text-black font-bold">Apple iPhone 14 Pro Max</p>
          <span class="text-black bg-transparent border border-gray-200 rounded-lg py-2 px-4">
            From $999
          </span>
          <hr class="border border-gray-200 w-full" />
          <p class="text-black font-thin">Deal ends in:</p>
        </div>
        <div class="lg:max-w-[640px] lg:max-h-[640px] bg-gray-200/[.2] rounded-lg">
          <img
            src="/assets/img/telovendope_iphone_14_pro_max.webp"
            alt="iPhone 14 Pro Max"
          />
        </div>
        <div class="flex flex-col gap-6">
          <h1 class="text-black text-2xl">Apple iPhone 14 Pro Max</h1>
          <p class="font-thin text-gray-400 text-sm">
            While most people enjoy casino gambling, sports betting, lottery and
            bingo playing for the fun.
          </p>
          <div class="flex flex-row gap-4"></div>
        </div>
      </section>
    </div>
  );
};
