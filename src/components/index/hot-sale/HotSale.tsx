import { ProductCard } from '@/shared/product/ProductCard';
import { Countdown } from '@/shared/countdown/Countdown';
import { CardType } from '@/infrastructure/enum/CardType';

export type ProductResponse = {
  price: number;
  title: string;
  img: string;
};

export interface HotSaleProps {
  products: ProductResponse[];
}

export const HotSale = ({ products }: HotSaleProps) => {
  return (
    <div class="flex flex-col gap-10 lg:my-[120px] lg:p-0 p-4">
      <div class="flex lg:flex-row flex-col gap-4 items-center">
        <h1 class="text-black text-2xl font-semibold">🔥 Hot Deal Today</h1>
        <Countdown />
      </div>
      <div class="grid lg:grid-cols-6 md:grid-cols-3 grid-cols-2 gap-6">
        {products.map((product: ProductResponse) => (
          <ProductCard
            product={product}
            type={CardType.NORMAL_CARD}
            key={Math.random()}
          />
        ))}
      </div>
    </div>
  );
};
