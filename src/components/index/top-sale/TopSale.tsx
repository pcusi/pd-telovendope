import { ProductCard } from '@/shared/product/ProductCard';
import { CardType } from '@/infrastructure/enum/CardType';
import { ProductResponse } from '@/components/index/hot-sale/HotSale';

export interface TopSaleProps {
  products: ProductResponse[];
}

export const TopSale = ({ products }: TopSaleProps) => {
  return (
    <div class="flex flex-col gap-10 lg:my-[120px] lg:p-0 p-4">
      <h1 class="text-black xl:text-2xl font-semibold">Top Products</h1>
      <div class="grid lg:grid-cols-4 gap-6">
        {products.slice(0, 4).map((product: ProductResponse) => (
          <ProductCard
            type={CardType.TOP_CARD}
            product={product}
            key={Math.random()}
          />
        ))}
      </div>
      <div class="grid grid-cols-1 gap-6 lg:grid-cols-2">
        <div class="flex flex-col p-10 gap-10 bg-gray-200/[.2] rounded-xl">
          <div class="lg:max-h-[490px] max-h-[425px]">
            <img
              class="h-full object-cover self-center"
              src="/assets/img/telovendope_apple_tv.webp"
              alt="Telovendope Apple TV"
            />
          </div>
          <div class="flex lg:flex-row flex-col justify-between items-start lg:items-center lg:gap-0 gap-10 lg:mt-auto">
            <div class="flex flex-col gap-2">
              <h1 class="text-black font-semibold text-xl">Sony Playstation</h1>
              <p class="text-gray-400 font-thin">$129</p>
            </div>
            <p class="text-black text-sm font-semibold lg:m-0 ml-auto">
              More Details
            </p>
          </div>
        </div>
        <div class="grid grid-cols-1 gap-6">
          <ProductCard type={CardType.TOP_CARD_HORIZONTAL} />
          <ProductCard type={CardType.TOP_CARD_HORIZONTAL} />
        </div>
      </div>
    </div>
  );
};
